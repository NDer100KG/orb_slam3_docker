# ORB-SLAM3 in docker

## Requirements
1. `nvidia-docker2`
    * [Installation guide](https://k2r2bai.com/2018/02/17/container/nvidia-docker-install/)
2. docker image rather from 
    * docker hub => I'm using image from `nvidia/cudaGL` and install ros on my own
        * note: To use rviz, we need OpenGL, which is supported in `nvidia/cudagl`, instead of `nvidia/cuda`
    * Dockerfile
        * Dockerfile_ros => for building a cudagl + ROS kinetic image
        * Dockerfile_ORBSLAM => for building a ORB_SLAM3 environment image
        * Dockerfile => A combination of two other Dockerfiles

## Build from docker hub image
1. Run base docker using `nvidia/cudagl`
    ```
    docker run -it 
        -e DISPLAY=${DISPLAY} \
        -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
        -v <path to ORB_SLAM3>:/tmp/ORB_SLAM3 \
        -v <path to dataset>:/tmp/dataset \
        --runtime=nvidia \
        --name <name> 
        nvidia/cudagl:lastest
    ```

    for example:
    ```
    docker run -it \
        --runtime=nvidia \
        -e DISPLAY=${DISPLAY} \
        -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
        -v /home/ente_chou/Documents/TEST123/ORB_SLAM3:/tmp/ORB_SLAM3 \
        -v /media/ente_chou/DDData/openvins_datasets:/tmp/dataset \
        nvidia/cudagl:11.2.1-base-ubuntu16.04
    ```
3. give docker access to display
    ```
    xhost +local:docker
    ```

5.  Install Ros kinetic
    ```
    (container) > apt-get update && apt-get install -y lsb-release && apt-get clean all
    (container) > sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    (container) > apt install curl && curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
    (container) > apt-get update && apt-get install -y ros-kinetic-desktop
    ```
6. Open another terminal attaching to the container to run roscore

    ```
    > docker exec -it <name> bash
    (container) > roscore
    ```
7. And Another terminal to run rviz
    ```
    > docker exec -it <name> bash
    (container) > rviz
    ```

6. Install ORB_SLAM3 dependencies, see ORB_SLAM3 repo
7. Build ORB-SLAM3
    * if encountered `double-check your ROS_PACKAGE_PATH` error:
    ```
    export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:${PWD}/Examples/ROS
    ```
8. Enjoy it


## Build from Dockerfile
1. Build the docker file:
    ```
    docker build -t <IMAGE_REPO>:<IMAGE_TAG> .
    ```
2. Run docker
    ```
    docker run -it 
        -e DISPLAY=${DISPLAY} \
        -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
        -v <path to ORB_SLAM3>:/tmp/ORB_SLAM3 \
        -v <path to dataset>:/tmp/dataset \
        --runtime=nvidia \
        --name <name> 
         <IMAGE_REPO>:<IMAGE_TAG> 
    ```
7. Build ORB-SLAM3
    * if encountered `double-check your ROS_PACKAGE_PATH` error:
    ```
    export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:${PWD}/Examples/ROS
    ```

4. Enjoy it