FROM nvidia/cudagl:10.1-runtime-ubuntu16.04

RUN apt update && \
    apt-get update && apt-get install -y lsb-release && apt-get clean all && \
    sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' && \
    apt install -y curl && curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - && \
    apt-get update && apt-get install -y ros-kinetic-desktop && \
    echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc && \
    apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential && \
    rosdep init && \
    rosdep update && \
    apt update && \
    apt install -y gitk && \
    apt install -y cmake && \
    apt install -y libglew-dev && \
    git clone https://github.com/stevenlovegrove/Pangolin.git && \
    cd Pangolin && mkdir build && cd build && cmake .. && cmake --build . && \
    apt install libeigen3-dev


